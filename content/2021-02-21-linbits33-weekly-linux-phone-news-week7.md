+++
title = "LinBits 33: Weekly Linux Phone news / media roundup (week 7)"
aliases = ["2021/02/21/linbits33-weekly-linux-phone-news-week7.html", "linbits33"]
author = "Peter"
date = "2021-02-21T22:58:00Z"
layout = "post"
[taxonomies]
authors = ["peter"]
tags = ["PINE64", "PinePhone", "Purism", "Librem 5", "LINMOBapps", "PINE64 Community Update", "Fxtec Pro 1X", "OpenSUSE", "Megapixels", "ExpidusOS", "Manjaro", "Lomiri", "Plasma Mobile"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

Megapixels scans QR code, Plasma 5.21, the new default PinePhone OS and more! _Commentary in italics._
<!-- more -->

### Software development and releases

#### Apps
* [Jumpdrive 0.7](https://github.com/dreemurrs-embedded/Jumpdrive/releases/tag/0.7) has been released, adding support for the Librem 5.
* [Plasma Mobile 5.21](https://kde.org/announcements/plasma/5/5.21.0/) is out.
* [Megapixels 0.15.0 has been released](https://git.sr.ht/~martijnbraam/megapixels/refs/0.15.0). _Features include graphicsmagick and QR code support._

#### Distributions
* Some new OpenSUSE images for the PinePhone [are available](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/?C=N;O=D). 
* [ExpidusOS](https://expidusos.com) have released [initial images](https://build.expidusos.com) of their Xfce based distribution.

### Worth noting
* In some way [Linux on mobile devices dominates planet Mars now](https://www.theverge.com/2021/2/19/22291324/linux-perseverance-mars-curiosity-ingenuity).
* The [Sailfish Browser is working on Mesa now](https://twitter.com/adampigg/status/1362725713649029120), meaning it can work on the PinePhone (and other possible mainline Linux devices) now.
* Initial Librem5 [front-camera enablement is happening](https://social.librem.one/@dos/105751707781287952).

### Worth reading 
* PINE64: [February Update: Show and Tell](https://www.pine64.org/2021/02/15/february-update-show-and-tell/). _This is a monster update, as always. If you don't have the time to read it all, just watch PizzaLovingNerds awesome video, linked below. The most important announcements for this blog are Manjaro Plasma Mobile as a default OS and more details on the keyboard cover, which looks really nice._
* OMG!Ubuntu: [PinePhone Picks a Default OS — But Not the One I Expected](https://www.omgubuntu.co.uk/2021/02/pinephone-picks-manjaro-with-plasma-mobile-as-default-os). _Joey's take._
* Linux Smartphones: [Future PinePhones will ship with Manjaro and Plasma Mobile (and other updates on the keyboard, modem firmware, and more)](https://linuxsmartphones.com/future-pinephones-will-ship-with-manjaro-and-plasma-mobile-and-other-updates-on-the-keyboard-modem-firmware-and-more/). _Brad's take._
* Linux Smartphones: [DIY smartphone keyboard made from salvaged Psion 5 PDA parts](https://linuxsmartphones.com/diy-smartphone-keyboard-made-from-salvaged-psion-5-pda-parts/). _Nice! Remember my [stupid simple version](https://devtube.dev-wiki.de/videos/watch/70e4e4bb-291e-42eb-aca7-804763e839ff) of this?_
* Linux Smartphones: [F(x)tec Pro1-X will ship in August with a Snapdragon 662 processor rather than March with an SD835 chip](https://linuxsmartphones.com/fxtec-pro1-x-will-ship-in-august-with-a-snapdragon-662-processor-rather-than-march-with-an-sd835-chip/). _Sad to see this delay. I expect this to change the mainline Linux story for the worse._
* Mobian Blog: [What is Mobian](https://blog.mobian-project.org/posts/2021/02/18/what_is_mobian/). _Great overview!_
* Official Jolla Blog: [Fourth generation of Sailfish OS is here!](https://blog.jolla.com/sailfish4/). _This is the official announcement of Koli, the new release of Sailfish OS._
* Jason Santa: [Applications that I use on my pinephone](https://jasonsanta.xyz/blog/applications-that-i-use-on-my-pinephone/). _This is mostly a list, but he's going to follow up on this by explaining how this works._
* TuxPhones: [Manjaro ships Plasma Mobile 5.21, adds initial setup for PinePhone](https://tuxphones.com/manjaro-mobile-initial-setup-ui-plasma-mobile-5-21-pinephone/). 

### Worth listening
postmarketOS Podcast: [#3 Librem 5, Mainlining, Feedbackd, Plasma 5.21](https://cast.postmarketos.org/episode/03-Librem5-Mainlining-Feedbackd-Plasma521/). _Great episode. Give it a listen!_

### Worth watching
* PINE64: [February Update: Show and Tell](https://odysee.com/@PINE64:a/february-update-show-and-tell:a). _Great video by PizzaLovingNerd!_
* Charbax: [Secure Linux Messenger: Arma Instruments G1 Secure Communicator](https://www.youtube.com/watch?v=p-HzESA-n0c). _This is an interesting device._
* Elatronion: [How To Install Different OSes On The PinePhone! + Mobian Overview](https://www.youtube.com/watch?v=u65dofYFAPY). _Nice, helpful video by my buddy Ezra._
* Linux Lounge: [Gaming on the PinePhone Using Ubuntu Touch!](https://peertube.co.uk/videos/watch/daa97d7b-1d51-44a6-8cc9-726f1b5aacb3). _Great one for everybody who has time for gaming!_
* PizzaLovingNerd: [Plasma Mobile is Maturing Well (PinePhone OSes: Manjaro Plasma)](https://www.youtube.com/watch?v=26AzFbFRXYE). _Nice video by PizzaLovingNerd!_
* HomebrewXS: [Manjaro Plasma Mobile Overview](https://www.youtube.com/watch?v=OCQLSGh2eDE). _Nice overview of Manjaro Plasma Mobile._
* HomebrewXS: [XFCE on the PinePhone, ExpidusOS](https://www.youtube.com/watch?v=i9Xe-r3WHLE). _Great first look at ExpidusOS, a Void Linux fork with Xfce for the PinePhone. I tried it too, and once it feels like it's actually close to being usable, I am going to make a video about this._

#### postmarketOS corner
* Felix Pojtinger: [Installing postmarketOS on the BQ Aquaris X5](https://www.youtube.com/watch?v=4NpS1uqTjlQ). _Awesome explanatory video on how to get postmarketOS to run the bq Aquaris X5. Keep in mind that neither sound nor the battery indicator work out of the box though on the bq Aquaris X5._
* Martijn Braam: [Megapixels 0.15.0, QR support](https://www.youtube.com/watch?v=QV1iovdFaDs). _Short video demoing the new QR code scanning feature on Megapixels._
* Martijn Braam: [Hammerhead running smoother with new GPU patches](https://www.youtube.com/watch?v=BFPl2Sabj38). _This is a Nexus 5 running postmarketOS fairly nicely! Awesome!_


#### Security corner
* Privacy & Tech Tips: [MAC/SSID Location Tracker Functional On Pinephone + Other Uses + Dev Reconnects Give Away Handshakes](https://tube.tchncs.de/videos/watch/9e9e577e-2ed8-4866-9d1c-e7066a0274c3).
* Privacy & Tech Tips: [Phone WiFi On? = Identifies GPS/Home Address; Py Mactrack Monitor Logging & Location Identification](https://tube.tchncs.de/videos/watch/8550c86f-472c-4f61-ae2a-f35359ee061c).
* Privacy & Tech Tips: [Identify Masked Burglar With Button On Your Linux Phone (Pinephone Shown)](https://www.youtube.com/watch?v=Qr-tTC5nQKE). _All of these are really interesting. I learnt a lot &mdash; I roughly knew that this is all possible, but these videos show how to do it._

### Stuff I did

#### Content

I published one video this week. It's called "Manjaro Daily Dev Builds for PinePhone &mdash; a look at Lomiri, Phosh and Plasma Mobile", and you can watch it on
* [PeerTube](https://devtube.dev-wiki.de/videos/watch/95834c0c-7416-46cf-ac7f-46677a92acc2)
* [Odysee](https://odysee.com/@linmob:3/manjaro-daily-dev-builds-for-pinephone-a:1) or
* [YouTube](https://www.youtube.com/watch?v=cW224e-4ZYE).

I also did a live stream on YouTube sharing some early impressions of the Librem 5. If you want to watch it now, you can do so on [Odysee](https://odysee.com/@linmob:3/linmoblive3-librem5-and-pinephone:4). You'll need an account though and it costs 2 fantasy crypto coins. _Some people have been upset about this. My reasoning behind this is that I don't want my streams to get to many views after the fact, as they are basically unedited impressions. That said, while the video was not private on YouTube, it got (stream and slight edit combined) 380 views (I was waiting for the video to sync over to Odysee, until comments complaining about "silent audio" led to me taking it private), so I think my reasoning is not totally absurd._


#### Random

I [played with Expidus OS for a bit](https://fosstodon.org/@linmob/105764335621702976). It needs time, but I like the idea a lot.


#### LINMOBapps

This week I added three apps to LINMOBapps, [and some maintenance happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). I also added some more [PKGBUILDs](https://framagit.org/linmobapps/linmobapps.frama.io/-/tree/master/PKGBUILDs). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)! 

