+++
title = "Samsung GT-i8320 - Impressions of the LiMo compliant Vodafone 360 OS"
aliases = ["2011/03/27/samsung-gt-i8320-impressions-of-the-limo-compliant-vodafone-360-os.html","2011/03/samsung-gt-i8320-impressions-of-limo.html"]
author = "peter"
date = "2011-03-27T17:35:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "h1droid", "LiMo", "MeeGo", "open source", "openness", "Samsung GT-i8320", "SHR"]
categories = ["hardware", "impressions"]
authors = ["peter"]
+++
Of course I didn't get the Samsung H1 / GT-i8320 to use it with the preinstalled system, a LiMo compliant system made customized for the 360 service of Vodafone (an European operator), I bought one to play with the original software, bought it to try MeeGo, Android, SHR and other software on this. 
<!-- more -->
But right now I am using it with the original LiMo compliant software - features like FM Radio or the quite ok camera don't work on Android yet (H1droid, which, btw, was sluggish from time to time (not due to RAM shortage)) and they are nice to have.

{{ gallery() }}

In fact, the original OS on the H1 is not too bad. Its UI is certainly not for business use, rather for the young, social animal. Nontheless the Facebook application for the 360 platform is disappointing, and I didn't find a twitter client (and there won't be one, as the 360 platform has been discontinued by Vodafone).

It's not the first time that I want to write: &#8220;Good ideas poorly carried out.&#8221; In a way this is different though. As there is no public SDK for native software (only a SDK for widgets), you don't find much software - some doesn't work (I tried to run a chinese FBreader port, which didn't install).. nontheless, out there are tons of applications that could run on this LiMo r2 compliant software platform, as it is build around GTK+ and clutter. And that's not all: Some J2ME apps don't run well, permission settings for these are crippled and makes using some of these painful (read: those requiring permissions, e.g. for web access).

Think of Abiword embedded, fbreader-gtk, pidgin or gwibber or the GPE PIM apps - great opportunities missed due to the closed nature of a platform, that was sold as a smartphone platform, but in reality, as it exists, offers no more than a feature phone platform would. 

As you may have guessed already, I will spend my free time (as soon as I will have free time again) to try porting those FOSS applications to the LiMo compliant 360 OS.

If you love videos, I <a href="http://www.youtube.com/watch?v=tR99GXSpSgE">recommend this video review by phonearena.com</a>.
