+++
title = "LinBits 40: Weekly Linux Phone news / media roundup (week 14)"
aliases = ["2021/04/11/linbits40-weekly-linux-phone-news-week14.html", "linbits40"]
layout = "post"
date = "2021-04-11T21:36:00Z"
author = "Peter"
[taxonomies]
authors = ["peter"]
tags = ["PINE64", "PinePhone", "Purism", "Librem 5", "postmarketOS", "PineTalk", "Shortwave", "Tweaks", "LG", "Mobian", "JingOS", "Ubuntu Touch", "PureOS", "convergence"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

A new postmarketOS Tweak tool, Shortwave 2.0, JingOS on Smartphones and more! _Commentary in italics._
<!-- more -->


### Software development and releases
* [postmarketOS Tweaks](https://gitlab.com/postmarketOS/postmarketos-tweaks) has been [unveiled](https://fosstodon.org/@postmarketOS/106038361436651318), a GNOME Tweaks like app for postmarketOS. _This is great, and I hope that other distributions follow and add a few more features like e.g. scaling._
*  Felix Häcker: [New Shortwave release](https://blogs.gnome.org/haeckerfelix/2021/04/09/new-shortwave-release/). _I already updated the LINMOBapps listing._


### Worth noting
* FOSS2go: [LG will open the source code of their phones? [Petition]](https://foss2go.com/lg-will-open-the-source-code-of-their-phones-petition/). _LG have announced that they will be leaving the smartphone market, this petition asks them to not turn their phones into e-waste._

### Worth reading 

* Mobian blog: [The road to the Mobian Community Edition (part 1)](https://blog.mobian-project.org/posts/2021/04/08/road-to-mobian-1/). _Looking forward to part 2!_
* Linux Smartphones: [Video: Linux-based tablet operating system JingOS is coming to phones](https://linuxsmartphones.com/video-linux-based-tablet-operating-system-jingos-is-coming-to-phones/). _This is, as JingOS in general, quite interesting. It's still early days, no portrait mode yet. If you're interested in more on JingOS, make sure to watch this [great video](https://www.youtube.com/watch?v=u_RVVTMzKYE)._
  * OMG!Ubuntu: [Is a JingOS Linux Phone Coming? This Video Hints at the Possibility](https://www.omgubuntu.co.uk/2021/04/jingos-linux-phone-video-demo). _Joey's take._
* Purism: [Librem 5 News Summary: March 2021](https://puri.sm/posts/librem-5-news-summary-march-2021/). _A lot of progress. Sadly nothing definitive on future deliveries, yet._
* Gamey: [5 amazing PINEPHONE& Librem5 Apps [DanctNIX Arch ARM]](https://odysee.com/@gamey:c/5-best-pinephone-apps:3). _Nice post about 5 actually great apps!_


### Worth listening
* PineTalk [006: Pine for You](https://www.pine64.org/2021/04/10/006-pine-for-you/) _Ezra and I made another one of these. I hope listening is as much fun as is making it. And please ask a few questions or give us other food for thought we can discuss!_

### Worth watching
* Purism: [The Simplicity of Making Librem 5 Apps](https://www.youtube.com/watch?v=9qB_5g2ZJYk). _This is why I am so excited for Linux phones._
* Martijn Braam: [postmarketOS tweaks GTK app](https://www.youtube.com/watch?v=X_QuQKhEVRA). _Nifty app!_
* Calling All Platforms Tech: [PinePhone Review](https://www.youtube.com/watch?v=1l6f5Sngk58). _Nice review._
* Privacy & Tech Tips: [Pinephone/Linux Security: Create A Hidden Encrypted Volume Inside A Video File Using ZuluCrypt](https://odysee.com/@RTP:9/pinephone-demo-create-a-hidden-encrypted:1). _Nice idea! Now the PinePhone only needs proper video recording, so that this is more plausible. ;-)_
* Privacy & Tech Tips: [Linux Lesson: Bash Scripting: Taking Input/Output, Variables, Pipes, Background + Automatic Upgrades](https://tube.tchncs.de/videos/watch/e1de5c42-e7c6-4b7d-aaf7-d7a3f415f085). _Not super phone related, but totally warranted and a great resource._
* E. Matt Armstrong: PinePhone Arch Distro Spin [Part 2 Apps and Usage](https://www.youtube.com/watch?v=0mif8Pco96w), [Part 3 yay the AUR helper](https://www.youtube.com/watch?v=ZUsABNqy5Xk), [Part 4 Installing from the AUR](https://www.youtube.com/watch?v=CE-tm8XwZbQ). _Great to see [old tutorial posts](https://linmob.net/2020/09/05/pinephone-building-plasma-mobile-apps-from-the-aur.html) being used!_ 
*  Sk4zZi0uS: [PinePhone Challenge 1 week update (software I use, tricks and fanboying) - 2021-04-08](https://www.youtube.com/watch?v=YT4FWoV5U2E). _I did not manage to watch the entirety of this, but I must say that it's brave to choose Plasma Mobile at this point in my most honest opinion. I guess I need to go back to it and see how it is these days._
* JingOS: [Developer Update | JingOS v0.6 first boot demo on a smartphone](https://www.youtube.com/watch?v=JsEd3-O8fR8), [Developer Update | First Multimedia demo on JingOS ARM smartphone](https://www.youtube.com/watch?v=4MIjCV1VU4E). _That device reminds me a lot of the Gigaset GS290 I have, but I've seen plenty other devices using similar looking screens. Most likely they are using a Unisoc SoC here, too, which likely is a clear hint at a vendor kernel/libhybris based stack._
* UBports: [Ubuntu Touch Q&A 98](https://www.youtube.com/watch?v=BCpma7A-TjI). _Another Q&A, I bet it's interesting&mdash;sadly I did not manage to watch it yet._

### Stuff I did

#### Content
None, sorry. 

#### Random
* I switched back to DanctNIX, this time [full disk encrypted](https://github.com/dreemurrs-embedded/archarm-mobile-fde-installer), after managing to blow up my pmOS distribution on an upgrade.
* I've played with [Librem 5 convergence running PureOS Byzantium](https://social.librem.one/@dos/106047622392779252), and [had to fool around with Plasma](https://fosstodon.org/@linmob/106047443542616337).
* I've also started working on migrating this blog over to [Zola](https://getzola.org), another static site generator. I hope to be done with that before May.


#### LINMOBapps

I added three apps to LINMOBapps, so that we have a total of 249 apps right now: postmarketOS Tweaks (see above); Public Transport, a public transport companion for Germany; and Simple Diary, a markdown-supporting diary app. [See here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)! 
